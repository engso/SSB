# SSB

[![Documentation Status](https://readthedocs.org/projects/ssb/badge/?version=latest)](https://ssb.readthedocs.io/en/latest/?badge=latest)

Solving Stratzenblitz permanent docs.


## Contributing

For quick typo fixes, you can make edits on the [GitLab web interface](https://gitlab.com/SIGSTACKFAULT/SSB/). -- Click the "Edit on GitLab" button in the top-right of a page.

For more complicated stuff (moving files, etc.), you'll need to install Git.

**This is not a Git tutorial. If you've never heard of Git, you probably shouldn't do this.**

1. [Enable WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
2. [Download Ubuntu](https://www.microsoft.com/en-ca/p/ubuntu-1804-lts/9n9tngvndl3q?rtc=1&activetab=pivot:overviewtab), or your preferred distro
3. Type "ubuntu" into the start menu
4. In the terminal window, type `sudo apt install git`
5. `git clone https://gitlab.com/SIGSTACKFAULT/SSB/`