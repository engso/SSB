Stock Parts Monorail
====================

.. video:: svhHcHdPgeA
   :frame: 2:02

2:02
----

**Stratish:** `I wait in scilence... somehow my fear seem empty`

**Link:** `Deja vu on the Drift Express - YouTube <https://www.youtube.com/watch?v=l_3Nhbn1Mx8>`_

    *You have any idea how hard it is to drive a vehicle across ice,*
    *nevermind one with almost no grip and you can't steer,*
    *only control the throttle?* -- SIGSTKFLT, YouTube comment

.. admonition:: TODO
   :class: danger

   Double-check translation
