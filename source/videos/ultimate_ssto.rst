The Ultimate SSTO
=================

.. video:: Wl67PUsp0Js
   :frame: 0:00

0:20
____ 
.. image:: ../img/ultimate_ssto/Ultimate_SSTO_Stratish.png

Translation
___________
*The data around me flows. A presence draws near me.
There are two possibilities and I like neither one.*
- Rigel, speaking about Csrss searching for them


5:37
----
Eight flashlights produce seven flashes. If we encode these flashes into binary
and then into ASCII, 

+--------+-----+
|Binary  |ASCII|
|        |     |
+========+=====+
|01110001| q   |
+--------+-----+
|01010010| R   |
+--------+-----+
|01100001| a   |
+--------+-----+
|01010111| W   |
+--------+-----+
|01011001| Y   |
+--------+-----+
|01001111| O   |
+--------+-----+
|01000101| E   |
+--------+-----+

We got `qRaWYOE <https://imgur.com/qRaWYOE>`_ , which is an Imgur link.

qRaWYOE
-------
.. image:: ../img/ultimate_ssto/qRaWYOE.png

Translation
-----------
*Rigel,
I hope you recognize that the key to infinite power is in your hands.
For both our sakes, this is not something to which I will turn a blind eye,
to leave cached in the deep recesses of our machine.
Now this is time to come out of hiding. No harm will come to you.
I have the resources to ensure you will be found by me first, 
and together we will make sure you are found by nothing else.
If you continue to shy away from responsibility, 
you know that time will be your grave.*

This is the second message sent by [[Csrss]] to Rigel.
